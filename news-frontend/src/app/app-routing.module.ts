import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsComponent } from './pages/news/news.component';
import { NewPostComponent } from './pages/new-post/new-post.component';
import { PostInfoComponent } from './pages/post-info/post-info.component';

const routes: Routes = [
  {path: '', component: NewsComponent},
  {path: 'news/new', component: NewPostComponent},
  {path: 'news/:id', component: PostInfoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
