export class Comment {
  constructor(
    public id: string,
    public item_id: string,
    public author: string,
    public comment: string,
  ) {}
}

export interface CommentData {
  [key: string]: any;
  item_id: string;
  author: string;
  comment: string;
}
