import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NewsService } from '../../services/news.service';
import { Router } from '@angular/router';
import { NewsData } from '../../models/news.model';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.sass']
})
export class NewPostComponent implements OnInit {
  @ViewChild('f') form! : NgForm;

  constructor(private newsService: NewsService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const newsData: NewsData = this.form.value;
    this.newsService.createNews(newsData).subscribe(() => {
      void this.router.navigate(['/']);
    })

  }
}
