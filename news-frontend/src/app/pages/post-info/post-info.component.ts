import { Component, OnInit, ViewChild } from '@angular/core';
import { NewsService } from '../../services/news.service';
import { Comment, CommentData } from '../../models/comment.model';
import { CommentsService } from '../../services/comments.service';
import { News, NewsData } from '../../models/news.model';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-info',
  templateUrl: './post-info.component.html',
  styleUrls: ['./post-info.component.sass']
})
export class PostInfoComponent implements OnInit {
  @ViewChild('f') form! : NgForm;

  comments: Comment[] = [];

  news: News[] = [];

  constructor(private commentsService: CommentsService, private newsService: NewsService, private router: Router) { }

  ngOnInit(): void {
    this.commentsService.getComments().subscribe(comments => {
      this.comments = comments;
      console.log(this.comments)
    })
    this.newsService.getNews().subscribe(news => {
      this.news = news;
    })
  }

  onSubmit() {
    const commentData: CommentData = this.form.value;
    this.commentsService.createComment(commentData).subscribe(() => {
      void this.router.navigate(['/']);
    })
  }
}
