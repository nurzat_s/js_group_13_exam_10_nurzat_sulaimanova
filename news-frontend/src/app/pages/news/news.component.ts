import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../services/news.service';
import { News } from '../../models/news.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.sass']
})
export class NewsComponent implements OnInit {
  post!: News;
  news: News[] = [];

  constructor(private newsService: NewsService, private router: Router,private route: ActivatedRoute ) { }

  ngOnInit(): void {
    this.newsService.getNews().subscribe(news => {
      this.news = news;
      console.log(this.news)
    })
  }

  onRemove() {
    this.newsService.removePost(this.post.id).subscribe(() => {
      this.newsService.fetchPosts();
      void this.router.navigate(['..'], {relativeTo: this.route});
    });
  }

}
