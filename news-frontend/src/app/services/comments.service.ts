import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Comment, CommentData } from '../models/comment.model';


@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private http: HttpClient) { }

  getComments() {
    return this.http.get<Comment[]>(environment.apiUrl + '/comments').pipe(
      map(response => {
        return response.map(commentData => {
          return new Comment(
            commentData.id,
            commentData.item_id,
            commentData.author,
            commentData.comment,
          )
        })
      }));
  }

  createComment(commentData: CommentData) {
    const formData = new FormData();

    Object.keys(commentData).forEach(key => {
      if(commentData[key] !== null) {
        formData.append(key, commentData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/comments', formData);
  }
}
