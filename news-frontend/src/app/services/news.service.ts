import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, tap } from 'rxjs/operators';
import { News, NewsData } from '../models/news.model';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class NewsService {
  postRemoving = new Subject<boolean>();
  postsFetching = new Subject<boolean>();
  postsChange = new Subject<News[]>();
  private posts: News[] = [];

  constructor(private http: HttpClient) { }

  fetchPosts() {
    this.postsFetching.next(true);
    this.http.get<{[id: string]: News}>(environment.apiUrl + '/news')
      .pipe(map(result => {
        if(result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const postData = result[id];
          return new News(id, postData.title, postData.content, postData.image, postData.date);
        });
      }))
      .subscribe(posts => {
        this.posts = posts;
        this.postsChange.next(this.posts.slice());
        this.postsFetching.next(false);
      }, error => {
        this.postsFetching.next(false);
      });
  }
  fetchNews(id: string) {
    return this.http.get<News | null>(environment.apiUrl + '/news').pipe(
      map(result => {
        if(!result) {
          return null;
        }
        return new News(id, result.title, result.content, result.image, result.date);
      })
    );
  }

  getNews() {
    return this.http.get<News[]>(environment.apiUrl + '/news').pipe(
      map(response => {
        return response.map(newsData => {
          return new News(
            newsData.id,
            newsData.title,
            newsData.content,
            newsData.image,
            newsData.date
          )
        })
      }));
  }

  createNews(newsData: NewsData) {
    const formData = new FormData();

    const date = new Date;

    // newsData.date = date.toISOString();


    Object.keys(newsData).forEach(key => {
      if(newsData[key] !== null) {
        formData.append(key, newsData[key]);
      }
    });

    // formData.append('title', newsData.title);
    // formData.append('content', newsData.content);
    // formData.append('date', newsData.date);

    if(newsData.image) {
      formData.append('image', newsData.image);
    }

    return this.http.post(environment.apiUrl + '/news', formData);
  }

  removePost(id: string) {
    this.postRemoving.next(true);
    return this.http.delete(environment.apiUrl + '/news/:id').pipe(
      tap(() => {
        this.postRemoving.next(false);
      }, () => {
        this.postRemoving.next(false);
      })
    );
  }
}
