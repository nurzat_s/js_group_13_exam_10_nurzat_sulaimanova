const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT item.id, item.title, item.image, item.date FROM item';

    if (req.query.filter === 'image') {
      query += ' WHERE image IS NOT NULL'
    }

    if (req.query.direction === 'desc') {
      query += ' ORDER BY id DESC'
    }

    let [items] = await db.getConnection().execute(query);
    return res.send(items);
  } catch (e) {
    next(e);
  }

});

router.get('/:id', async (req, res, next) => {
  try {
    const [items] = await db.getConnection().execute('SELECT * FROM item WHERE id = ?', [req.params.id]);

    const item = items[0];

    if(!item) {
      return  res.status(404).send({message: 'Not found'});
    }

    return res.send(item);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if(!req.body.title || !req.body.content) {
      return res.status(400).send({message: 'Title, content are required'});
    }

    const item = {
      title: req.body.title,
      content: req.body.content,
      image: null,
      date: new Date().toISOString().substr(0, 10),
    };

    if(req.file) {
      item.image = req.file.filename;
    }

    let query = 'INSERT INTO item (title, content, image, date) VALUES (?, ?, ?, ?)';

    const [results] = await db.getConnection().execute(query, [
      item.title,
      item.content,
      item.image,
      item.date
    ]);

    const id = results.insertId;

    console.log(results);

    return res.send({message: 'Created new item', id});
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const [items] = await db.getConnection().execute('DELETE  FROM item WHERE id = ?', [req.params.id]);

    const item = items[0];

    if(!item) {
      return  res.status(404).send({message: 'Not found'});
    }

    return res.send(location);
  } catch (e) {
    next(e);
  }
});


module.exports = router;