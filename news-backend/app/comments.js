const express = require('express');
const db = require('../mySqlDb');

const router = express.Router();


router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT  * FROM comments';

    if (req.query.item_id) {
      query += ' WHERE item_id' + req.query.item_id;
    }


    let [comments] = await db.getConnection().execute(query);
    return res.send(comments);
  } catch (e) {
    next(e);
  }


});

router.get('/:id', async (req, res, next) => {
  try {
    const [comments] = await db.getConnection().execute('SELECT * FROM comments WHERE id = ?', [req.params.id]);

    const comment = comments[0];

    if(!comment) {
      return  res.status(404).send({message: 'Not found'});
    }

    return res.send(comment);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if(!req.body.title || !req.body.comment) {
      return res.status(400).send({message: 'Title and comment are required'});
    }

    const comment = {
      title: req.body.title,
      item_id: req.body,
      author: req.body.author,
      comment: req.body.comment,
    };

    let query = 'INSERT INTO comments (title, item_id, author, comment) VALUES (?, ?, ?, ?)';

    const [results] = await db.getConnection().execute(query, [
      comment.title,
      comment.item_id,
      comment.author,
      comment.comment
    ]);

    const id = results.insertId;

    console.log(results);

    return res.send({message: 'Created new comment', id});
  } catch (e) {
    next(e);
    return res.send({message: 'Error'});
  }
});
router.delete('/:id', async (req, res, next) => {
  try {
    const [comments] = await db.getConnection().execute('DELETE  FROM items WHERE id = ?', [req.params.id]);

    const comment = comments[0];

    if(!comment) {
      return  res.status(404).send({message: 'Not found'});
    }

    return res.send(comment);
  } catch (e) {
    next(e);
    return  res.send({message: 'Error on deleting'});
  }
});


module.exports = router;