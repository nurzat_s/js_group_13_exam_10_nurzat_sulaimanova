create schema news collate utf8_general_ci;
use news;

create table item
(
    id      int auto_increment
        primary key,
    title   varchar(255) not null,
    content varchar(255) not null,
    image   varchar(255) null,
    date    datetime     not null
);

create table comments
(
    id      int auto_increment
        primary key,
    item_id int          not null,
    author  varchar(255) null,
    comment varchar(255) not null,
    constraint comments_item_id_fk
        foreign key (item_id) references item (id)
            on update cascade on delete cascade
);

insert into comments (id, item_id, author, comment)
values  (1, 1, 'Peter', 'Art exhibition'),
        (2, 2, 'Kate', 'Football match');

insert into item (id, title, content, image, date)
values  (1, 'Art news', 'Smth about art', null, '2022-02-12 12:59:49'),
        (2, 'Sport news', 'About football', null, '2021-03-12 13:00:11');